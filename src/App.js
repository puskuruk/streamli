import React, {useState} from 'react'
import Header from './components/Header'
import Sidebar from './components/Sidebar'

import { Grid } from 'semantic-ui-react'
import DetailsScreen from './components/DetailsScreen'
import CurrentLiveStreams from './components/CurrentLiveStreams'

const App = () => {
  const [detailsScreen, goDetailsScreen] = useState(null)

  return(
    <React.Fragment>
      <header>
        <Header goDetailsScreen={goDetailsScreen} />
      </header>
      <main>
        <nav>
          <Sidebar />
        </nav>
        <div>
          {detailsScreen === null ? <CurrentLiveStreams goDetailsScreen={goDetailsScreen} /> : <DetailsScreen detailsScreen={detailsScreen} /> }
        </div>
      </main>
    </React.Fragment>
)
}
export default App