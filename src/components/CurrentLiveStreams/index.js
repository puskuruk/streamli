import React from 'react'
import StreamPreviewCard from '../StreamPreviewCard'
import { Card, Segment, Header } from 'semantic-ui-react'


const CurrentLiveStreams = ({goDetailsScreen}) => {
    return (
        <Segment>
            <Header as='h2'>Current Streams</Header>
            <Card.Group
                itemsPerRow={4}
            >
                <StreamPreviewCard goDetailsScreen={goDetailsScreen} />
                <StreamPreviewCard goDetailsScreen={goDetailsScreen} />
                <StreamPreviewCard goDetailsScreen={goDetailsScreen} />
                <StreamPreviewCard goDetailsScreen={goDetailsScreen} />
            </Card.Group>
        </Segment>
    )
}

export default CurrentLiveStreams