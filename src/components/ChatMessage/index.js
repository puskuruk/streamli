import React from 'react'
import { Feed } from 'semantic-ui-react'

const ChatMessage = ({message}) => (
  <div>
    <Feed>
      <Feed.Event>
        <Feed.Label image='https://placehold.it/50x50' />
        <Feed.Content>
          <Feed.Summary
            content={message}
            date={
              () =>{
                const currentDate = new Date()
                const currentHour = currentDate.getHours()
                const currentMinutes = currentDate.getMinutes()
                const currentSeconds = currentDate.getSeconds()
                return `${currentHour}:${currentMinutes}:${currentSeconds}`
              }}
          />
        </Feed.Content>
      </Feed.Event>
    </Feed>
  </div>
)

export default ChatMessage