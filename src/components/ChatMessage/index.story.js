import React from "react"

import { storiesOf } from "@storybook/react"
import ChatMessage from "."

storiesOf("ChatMessage", module).add("Single", () => (
  <div>
    <ChatMessage />
  </div>
))

storiesOf("ChatMessage", module).add("Multiple", () => (
  <div
    style={{
      height: '100px',
      overflow: 'auto'
    }}
  >
    <ChatMessage />
    <ChatMessage />
    <ChatMessage />
    <ChatMessage />
    <ChatMessage />
  </div>
))