import React from 'react'
import { Feed } from 'semantic-ui-react'

const CurrentLiveVideosFeedItem = () => (
    <Feed.Event>
      <Feed.Label image='https://placehold.it/50x50' />
      <Feed.Content>
        <Feed.Date content='3K people' />
        <Feed.Summary>
          MW2
        </Feed.Summary>
      </Feed.Content>
    </Feed.Event>
)

export default CurrentLiveVideosFeedItem