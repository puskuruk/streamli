import React from 'react'
import { Button, Icon, Transition } from 'semantic-ui-react'

const transitions = [
    'jiggle',
    'flash',
    'shake',
    'pulse',
    'tada',
    'bounce',
    'glow',
]

const DonationBox = ({setMadeDonation}) => {
    return (
        <div>
             <Button
                onCLick={() => setMadeDonation(true)}
                icon
             >
              <Icon name='money bill alternate outline' />
            </Button>   
        </div>
    )
}

export default DonationBox