import React from 'react'
import { Header, Icon, Image, Menu, Segment, Sidebar, Container } from 'semantic-ui-react'
import CurrentLiveVideosFeed from '../CurrentLiveVideosFeed'

const SidebarExampleVisible = () => (
          <Sidebar
            as={Menu}
            animation='uncover'
            icon='labeled'
            inverted
            vertical
            visible={true}
            width='thin'
          >
            <CurrentLiveVideosFeed />
          </Sidebar>
          
)

export default SidebarExampleVisible