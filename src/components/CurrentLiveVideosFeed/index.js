import React from 'react'
import { Card, Feed } from 'semantic-ui-react'
import CurrentLiveVideosFeedItem from '../CurrentLiveVideosFeedItem'


const CurrentLiveVideosFeed = () => (
  <Card
    style={{
      top: '240px'
    }}
  >
    <Card.Content>
      <Card.Header>Reent Streams</Card.Header>
    </Card.Content>
    <Card.Content>
      <Feed>
        <CurrentLiveVideosFeedItem />
        <CurrentLiveVideosFeedItem />
        <CurrentLiveVideosFeedItem />
        <CurrentLiveVideosFeedItem />
        <CurrentLiveVideosFeedItem />
      </Feed>
    </Card.Content>
  </Card>
)

export default CurrentLiveVideosFeed