import React, {useState, useEffect} from 'react'
import EmojiPicker from '../EmojiPicker'
import { Card, Input, Icon, Button } from 'semantic-ui-react'
import ChatMessage from '../ChatMessage'

import './chat-input.css'

const ChatBox = () => {
    const [currentConversation, setCurrentConversation] = useState([])
    const [currentText, setCurrentText] = useState("")
    
    const addEmoji = (event) =>{
        const emoji = event.native
        setCurrentText(currentText+emoji)
    }

    useEffect(() =>{
        setCurrentText("")
    },[currentConversation])

    return (
        <div
            className="full"
            style={{
                top: '100px'
            }}
        >
            <div className="chatbox-wrapper">
                {currentConversation}
            </div>

            <div className="chat-input">
                <Input type="text" value={currentText} onChange={event => setCurrentText(event.target.value)} />
                <div
                    style={{display:'flex'}}
                >
                    <Button>
                        <EmojiPicker addEmoji={addEmoji} />
                    </Button>
                    <Button
                        onClick={() => setCurrentConversation([...currentConversation, <ChatMessage message={currentText} />])}
                    >
                        <Icon name="send" color="teal" size="large" />
                    </Button>
                </div>
            </div>
        </div>
    )
}

export default ChatBox
