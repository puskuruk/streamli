import 'emoji-mart/css/emoji-mart.css'
import { Picker } from 'emoji-mart'
import { Popup, Button,Icon } from 'semantic-ui-react'

import './emoji-icon.css'

import React from 'react'

const EmojiPicker = ({addEmoji, emojiBoxIsOpen}) => {
    
    return (
            <Popup trigger={<Icon name="smile" color="yellow" className="emoji-icon" size="large" />} flowing hoverable>
                <Picker
                    set="twitter"
                    onSelect={addEmoji}
                />
            </Popup>
    )
}

export default EmojiPicker
