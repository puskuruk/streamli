import colors from './colors-array'

const pickRandomColor = () => {
    const colorIndex = Math.floor(Math.random() * colors.length)
    const color = colors[colorIndex]
    return color
}

export default pickRandomColor