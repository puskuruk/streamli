const k = 1000

const kCalculator = (numberWithoutDividedK) => {
    const numberDividedWithK = numberWithoutDividedK / k
    const stringKNumber = `${numberDividedWithK}k`

    return stringKNumber
}

export default kCalculator