import pickRandomColor from '../pick-random-color'
import colors from '../colors-array'

it('should return a color', () => {
    const color = pickRandomColor()
    expect(typeof color).toEqual('string')
})

it('should return index of color', () => {
    const color = pickRandomColor()
    const indexOfColor = colors.indexOf(color)
    expect(typeof indexOfColor).toEqual('number')
})