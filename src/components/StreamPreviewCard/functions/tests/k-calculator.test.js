import kCalculator from '../k-calculator'

it('should return string', () => {
    const numberWithK = kCalculator(1000000)
    expect(typeof numberWithK).toEqual('string')
})

it('should return string with k', () => {
    const numberWithK = kCalculator(1000000)
    const isNumberWithKHasK = numberWithK.includes("k")
    expect(isNumberWithKHasK).toEqual(true)
})