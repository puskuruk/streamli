import React from "react"

import { storiesOf } from "@storybook/react"
import StreamPreviewCard from "."

storiesOf("StreamPreviewCard", module).add("Basic", () => (
  <div>
    <StreamPreviewCard />
    <StreamPreviewCard />
    <StreamPreviewCard />
    <StreamPreviewCard />
  </div>
))