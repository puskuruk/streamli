import React, {useEffect, useState} from 'react'
import { Card,Image, Grid, Icon } from 'semantic-ui-react'

import pickRandomColor from './functions/pick-random-color'
import kCalculator from './functions/k-calculator.js'

const src = 'http://placehold.it/320x180'

const StreamPreviewCard = ({goDetailsScreen}) => {
    const [currentColor, setCurrentColor] = useState(null)
    const [currentPeopleWatchingCount, setCurrentPeopleWatchingCount] =useState(null)
    
    useEffect(() =>{
        const color = pickRandomColor()
        const peopleWatchingCount = kCalculator(10000)

        setCurrentPeopleWatchingCount(peopleWatchingCount)
        setCurrentColor(color)
    },[])
    
    return(  
        <Card
            color={ currentColor !== null ? currentColor : "teal" }
            onClick={() => goDetailsScreen(true)}
        >
            <Image src={src} />
            <Card.Content>
                <Card.Header>
                    <Image
                        src="http://placehold.it/50x50"
                        avatar
                    />
                    Resident Evil 2
                </Card.Header>
                
                <Card.Meta>puskuruk</Card.Meta>
                <Card.Description>

                </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <Icon name='user' />
              {currentPeopleWatchingCount !== null ? currentPeopleWatchingCount : 0}
          </Card.Content>
        </Card>
    )
}
export default StreamPreviewCard

{/* <Statistic
                horizontal
                size="mini"
            >
                <Statistic.Label>
                    <Icon name='user' />
                </Statistic.Label>
                <Statistic.Value>
                    {currentPeopleWatchingCount !== null ? currentPeopleWatchingCount : 0}
                </Statistic.Value>
            </Statistic> */}